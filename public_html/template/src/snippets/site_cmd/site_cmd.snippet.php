<?php
//recommend create a snippet called "site_cmd" and paste this in.
//Description: "Tools library class for small common snippet calls."
//Then cammands can be easily coded in in order for this to work [[!site_cmd? &json_options=`{"cmd":"output_mycontent"}`]]
if(isset($GLOBALS['walker_site_cmdObj'])===false){
	require_once MODX_BASE_PATH.'template/src/snippets/site_cmd/SiteCmd.class.php';
	$o = new SiteCmd($modx);
	$GLOBALS['walker_site_cmdObj'] = $o;
}else{
	$o=$GLOBALS['walker_site_cmdObj'];
}
if(isset($extraArg)===false){$extraArg=NULL;}
return $o->requestOutput($json_options,$extraArg);
?>