<?php

require_once MODX_BASE_PATH . 'core/components/commontools/data/DataProcess.class.php';

class StoreSearch {

    public $modx;
    private $a_storeData;
    private $s_csvPath;
    private $a_reqHeadKeys;
    private $lat;
    private $lon;

    function StoreSearch(&$modx) {
        $this->modx = $modx;
        $this->s_csvPath = MODX_BASE_PATH . 'client-assets/distributors/distributor_list.csv';
        $this->a_reqHeadKeys = array('Company', 'Address', 'Suburb', 'PC', 'State', 'Phone', 'Website', 'Longitude', 'Latitude');
        $this->populateData();
    }

    function populateData() {
        $s_csvData = file_get_contents($this->s_csvPath);

        $s_csvData = utf8_encode($s_csvData);

        $a_errors = array();
        $o_dataProcessor = new DataProcess();
        $processResult = $o_dataProcessor->csvStringToArray($s_csvData, $this->a_reqHeadKeys, $a_errors);

        if ($processResult === false) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, '[' . get_class($this) . ' Class] Error reading Bases csv data. ' . implode(', ', $a_errors));
            return '';
        }
        $this->a_storeData = $processResult;
    }

    function showSearch() {
        $s_ret = '<script type="text/javascript" src="[[!CommonTools? &cmd=`getHttpType`]]://maps.googleapis.com/maps/api/js?sensor=true"></script><script type="text/javascript" src="assets/js/StoreSearch.class.js"></script><script type="text/javascript" src="assets/js/StockistMap.class.js"></script><script type="text/javascript" src="assets/js/jquery-ui.min.js"></script>
            <script type="text/javascript">
                var GLOBAL_StoreSearch;
				var GLOBAL_StockistMap;
                $(document).ready(function(){
				
					GLOBAL_StockistMap = new StockistMap();
					GLOBAL_StockistMap.makeMap();
                    GLOBAL_StoreSearch = new StoreSearch(\'/storeprocessor\');
                    GLOBAL_StoreSearch.searchWithAddress();
           
});</script><div id="storeSearch" class="container"><div class="six columns alpha"><div>[[*content]]</div><div>Search via address: <input type="text" id="ss_address" name="address" /></div><div id="ss_store_list"></div></div><div class="ten columns omega" id="ss_map_canvas"></div><div id="ss_popup"><div id="ss_popup_inner"></div></div></div>';

        return $s_ret;
    }

    function getNearbyStores() {
        $a_stores = array();

        if (isset($_REQUEST['lon']) && $_REQUEST['lon'] != '' && isset($_REQUEST['lat']) && $_REQUEST['lat'] != '') {

            $this->lat = $_REQUEST['lat'];
            $this->lon = $_REQUEST['lon'];

            $a_others = array();
            $o_otherDists = array();

            foreach ($this->a_storeData as $store) {

                $distance = $this->latLonDistance($this->lat, $this->lon, $store['Latitude'], $store['Longitude']);

                if ($distance < 200) {
                    $i_index = intval($distance * 100000000000);
                    $a_others[$i_index] = array('store' => $store, 'dist' => $distance);
                    $a_otherDists[] = $i_index;
                }
            }

            $a_closestOthers = $this->getClosest($a_otherDists, $a_others, 30);
            $a_ret = array('others' => $a_closestOthers);
            return json_encode($a_ret);
        }
    }

    public function getClosest($a_distances, $a_objects, $i_number) {
        $a_closest = array();

        if ($a_distances != null) {
            if ($i_number > count($a_distances)) {
                $i_number = count($a_distances);
            }
            sort($a_distances);

            for ($i = 0; $i < $i_number; $i++) {
                $a_closest[] = $a_objects[$a_distances[$i]];
            }
        }
        return $a_closest;
    }

    public function latLonDistance($lat1, $lng1, $lat2, $lng2) {
        $pi80 = M_PI / 180;
        $lat1 *= $pi80;
        $lng1 *= $pi80;
        $lat2 *= $pi80;
        $lng2 *= $pi80;

        $r = 6372.797; // mean radius of Earth in km
        $dlat = $lat2 - $lat1;
        $dlng = $lng2 - $lng1;
        $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        $km = $r * $c;

        return $km;
    }

}
